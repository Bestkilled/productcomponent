
import java.util.ArrayList;


public class Product {
    private int id;
    private String name;
    private double price;
    private String image;

    public Product(int id, String name, double price, String image) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "Product{" + "id=" + id + ", name=" + name + ", price=" + price + ", image=" + image + '}';
    }
    public static ArrayList<Product> genProductList(){
        ArrayList<Product> list = new ArrayList<>();
        list.add(new Product(1,"Espresso",50,"1.jpg"));
        list.add(new Product(2,"Latte",55,"2.jpg"));
        list.add(new Product(3,"Americano",60,"3.jpg"));
        list.add(new Product(4,"Mocha",50,"4.jpg"));
        list.add(new Product(5,"Thai Tea",40,"5.jpg"));
        list.add(new Product(6,"Green Tea",55,"6.jpg"));
        list.add(new Product(7,"Milk",45,"7.jpg"));
        list.add(new Product(8,"Capuccino",60,"8.jpg"));
        list.add(new Product(9,"Milk Tea",55,"9.jpg"));
        list.add(new Product(10,"Mocchiato",70,"10.jpg"));
        return list;
    }
}
